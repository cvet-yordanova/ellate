import { getProductsByCategoryId } from "../../../services/product.service";
import productActionTypes from "../../products/product.types";
import categoryPageActionTypes from "./category-page.types";


export const loadCategoryPageProducts = (categoryId: string, currentState: any, dispatchFunction: Function) => {

	dispatchFunction({type: categoryPageActionTypes.LOAD_PRODUCTS_FOR_CATEGORY_PAGE});

	getProductsByCategoryId(categoryId)
		.then((res: any) => {
            const result = res.data ? res.data.getProductsByCategoryId : [];

            if(result) {
                dispatchFunction({type: productActionTypes.LOAD_PRODUCTS_SUCCESS, payload: result});
            }

            if(!currentState) {
                dispatchFunction({type: categoryPageActionTypes.LOAD_PRODUCTS_FOR_CATEGORY_PAGE, 
                    payload: {lastLoadedPage: 1, categoryId: categoryId, productIds: result.map((pr: any) => pr.id)}});
            }

		})
		.catch((err: any) => {
            //tstodo
            console.log(err, 'err')
            dispatchFunction({type: categoryPageActionTypes.LOAD_PRODUCTS_FOR_CATEGORY_PAGE_FAIL});
        });
};