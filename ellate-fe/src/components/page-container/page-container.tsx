import React from 'react';
import "./page-container.scss";

const PageContainer = (props: any) => {


    return (
        <div className="page-container">
            {props.children}
        </div>
    )
};

export default PageContainer;