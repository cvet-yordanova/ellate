import React from 'react';
import { useSelector } from 'react-redux';
import Table from '../../admin/components/table';
import PageLayout from '../../components/page-layout';
import './basket.component.scss';


const BasketPage = (props: any) => {
    const basketState = useSelector((state: any) => state.basket);

    const getTableSettings = () => {
        return {
            columns: {
                image: {title: 'Изображение'},
                name: {title: 'Артикул'},
                price: {title: 'Цена'},
                quantity: {quantity: 'Количество'},
                sku: {title: 'Sku'},
            }
        }
    }

    const getBasketProducts = () => {
        
         if (basketState && basketState.items) {
            let res = basketState.items.map((item: any) => {
               return {...item.product, quantity: item.quantity}
            })

            //tstodo
            console.log(basketState.items, 'res');

            return res;
        } else return []

    };


    return(
        <PageLayout>
            <Table data={getBasketProducts()} tableSettings={getTableSettings()} onEdit={() => {}}></Table>
        </PageLayout>
    )

}

export default BasketPage;