import { getProductById } from '../../services/product.service';
import { getProductsByCategoryId } from '../../services/product.service';
import productActionTypes from './product.types';


export const loadProductById = async (productId: string, dispatchFunction: Function) => {
	dispatchFunction({type: productActionTypes.LOAD_PRODUCT_BY_ID});

	return await getProductById(productId);
}

export const loadProductsByCategory = (categoryId: string, dispatchFunction: Function) => {
	dispatchFunction({type: productActionTypes.LOAD_PRODUCTS_BY_CATEGORY});

	getProductsByCategoryId(categoryId)
		.then((res: any) => {
            const result = res.data ? res.data.getProductsByCategoryId : [];
            dispatchFunction({type: productActionTypes.LOAD_PRODUCTS_BY_CATEGORY_SUCCESS, payload: result});
		})
		.catch((err: any) => {
            dispatchFunction({type: productActionTypes.LOAD_PRODUCTS_BY_CATEGORY_FAIL});
        });
};
