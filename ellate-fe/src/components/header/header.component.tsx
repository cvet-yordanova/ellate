import React from 'react';
import './header.styles.scss';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ReactComponent as UserIcon } from './../../assets/images/user.svg';
import { ReactComponent as PadlockIcon } from './../../assets/images/padlock.svg';

import { useSelector, useDispatch } from 'react-redux';
import { showLoginModal } from '../../redux/modals/modal.actions';

const Header = ({ currentUser, hidden }: any) => {
	const dispatch = useDispatch();
	const userState = useSelector((state: any) => state.user);

    const getUserName = () => {
        const {currentUser} = userState;
        return `${currentUser ? currentUser.firstName : ''} ${currentUser ? currentUser.lastName : ''}`
    }

	return (
		<div className="header">
			<div className="top-page-links d-flex jc-right">
				{!userState.currentUser ? (
					<div className="box d-flex ai-center">
						<div onClick={() => dispatch(showLoginModal())} className="auth-link d-flex mr-10">
							<UserIcon className="auth-icon mr-3" />
							<Link to="#">Вход</Link>
						</div>
						<div className="auth-link d-flex">
							<PadlockIcon className="auth-icon mr-3" />
							<Link to="/registration">Регистрация</Link>
						</div>
					</div>
				) : (
                    <div>{getUserName()}</div>
                )}
			</div>
			<div className="logo-page-row">
				<div className="logo-box">
					<p className="logo-text">ella.t</p>
				</div>
			</div>

			{/* {currentUser}
        <Link className="logo-container" to="/">
            <Logo className="logo"></Logo>
        </Link>
        <div className="options">
            <Link className="option" to="/shop">
                SHOP
            </Link>
            <Link className="option" to="/contact">
                CONTACT
            </Link>
            {
                currentUser ? 
                <div className="option" onClick={() => auth.signOut()}>
                    SIGN OUT
                </div> : 
                <Link to="/"></Link>
            }
            <Link className="option" to="/signin">
                SIGN IN
            </Link>
        </div>
        <CartIcon></CartIcon>

        {hidden ? null : <CartDropdown></CartDropdown>} */}
		</div>
	);
};

const mapStateToProps = () => ({
});

export default connect(mapStateToProps)(Header);
