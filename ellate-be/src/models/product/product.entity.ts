import { IsOptional, IsString } from 'class-validator';
import { IProduct } from 'src/interfaces/IProduct';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from '../category/category.entity';
Category
@Entity()
export class Product implements IProduct {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  name: string;

  @IsString()
  @Column()
  description: string;

  @ManyToOne(() => Category, (category) => category.products)
	@JoinColumn()
  category?: Category;

  @IsString()
	@Column()
  price: number;

  @IsOptional()
	@Column({ default: true })
  enabled: boolean;

  @IsOptional()
	@Column({ default: false })
  deleted: boolean;

  @IsString()
	@Column()
  sku: string;

  @IsString()
	@Column({nullable: true})
  featuredImage: string;
}
