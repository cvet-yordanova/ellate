import { apolloClient } from '..';
import { addProductToFavouritesMutation } from "../graphql/user.query";


export const addProductToFavourites = (productId: string) => {
    return apolloClient.mutate({
      mutation: addProductToFavouritesMutation,
      variables: {input: {id: productId}},
      optimisticResponse: true
    })
}