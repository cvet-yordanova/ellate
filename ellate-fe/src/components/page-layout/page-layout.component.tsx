import React from 'react';
import LoginModal from '../modals/login-modal';
import PageContainer from '../page-container';
import TopMenu from '../top-menu';
import Header from '../../components/header';
import Footer from '../footer';

const PageLayout = (props: any) => {
    return (
        <React.Fragment>
        <div>
            <LoginModal />
        </div>
        <Header />
        <TopMenu />
        <PageContainer>
            {props.children}
        </PageContainer>
        <Footer></Footer>
        </React.Fragment>
    )
}

export default PageLayout;

