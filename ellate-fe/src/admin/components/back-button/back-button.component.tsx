import React from 'react';
import { useHistory } from 'react-router-dom';


const BackButton = () => {

    let history = useHistory();

    return <div><p onClick={() => history.goBack()}>back</p></div>

}

export default BackButton;