import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import './category-form.styles.scss';
import { createCategory, updateCategory } from '../../../../services/category.service';
import { useQuery } from '@apollo/client';
import { getAllCategoriesQuery, getCategoryByIdQuery } from '../../../../graphql/category.query';
import { useHistory, useParams } from 'react-router-dom';
import { successNotification } from '../../../../services/notification-service';

const CategoryForm = () => {

	const history = useHistory();
	const { id } = useParams<{id?: string}>();

	const {loading: categoryLoading, error: categoryError, data: category} = useQuery(getCategoryByIdQuery, {
		variables: {id: id, options: {relations: ['category']}}
	});

	useEffect(() => {

		if(category && category.image) {
			setImg({
				src: category.image,
				alt: category.title
			});
		}

	}, [category])

	const [ file, updateFile ] = useState(null);
	const [ { alt, src }, setImg ] = useState({
		src: 'https://i.stack.imgur.com/y9DpT.jpg',
		alt: 'Upload an Image'
	});
	const { loading, error, data } = useQuery(getAllCategoriesQuery);

	function handleImageChange(event: any) {
		const { target: { value, files } } = event;
		const fileForUpload = files[0];

		if (fileForUpload) {
			setImg({
				src: URL.createObjectURL(fileForUpload),
				alt: fileForUpload.name
			});
		}

		updateFile(fileForUpload || value);
	}

	if(categoryLoading) {
		return <div>Loading Category</div>
	}

	if(category) {
		console.log(category, 'category')
	}

	if (data) {
		console.log(data, 'data');
	}

	let {getCategoryById} = category && id ? category : {getCategoryById: null};

	return (
		<div className="et0-form et0-admin-category-form">
			<h1 className="et0-form-label">Категория</h1>
			<Formik
				initialValues={{
					title: getCategoryById ? getCategoryById.title : '',
					slug: getCategoryById ? getCategoryById.slug : '',
					category: getCategoryById && getCategoryById.category ? getCategoryById.category.id : '',
					image: ''
				}}
				validate={(values) => {
					const errors: any = {};
					//tstodo
					return errors;
				}}
				onSubmit={(values, { setSubmitting, resetForm }) => {

					setSubmitting(true);

					if(getCategoryById && getCategoryById.id) {
						updateCategory({
							id: getCategoryById.id,
							title: values.title,
							slug: values.slug,
							category: values.category ? {id: values.category}: null,
							image: file
						}).then((res:any) => {
							if(res.data.updateCategory.affected) {
								const notification = { message: 'Category updated', title: 'Success', type: 'Success' };
								successNotification(notification);
							}
							history.push('/admin/categories');
						});
					} else {
						createCategory({
							title: values.title,
							slug: values.slug,
							category: values.category ? {id: values.category}: null,
							image: file
						}).then((res) => {
							if (res.data) {
								const notification = { message: 'Category saved', title: 'Success', type: 'Success' };
								successNotification(notification);
								resetForm({});
								updateFile(null);
								setImg({
									src: 'https://i.stack.imgur.com/y9DpT.jpg',
									alt: 'Upload an Image'
								});

								history.push('/admin/categories');
								
							}
						});
					}

				}}
			>
				{({ values, handleChange, isSubmitting }) => (
					<Form>
						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Категория
							</label>
							<Field className="et0-input-field" type="text" name="title" />
							<ErrorMessage name="title" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Slug
							</label>
							<Field className="et0-input-field" type="text" name="slug" />
							<ErrorMessage name="slug" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Категория
							</label>
							<select
								name="category"
								value={values.category}
								onChange={handleChange}
								style={{ display: 'block' }}
								className="et0-input-field"
							>
								<option value="null">-</option>
								{data && data.allCategories ? (
									
									data.allCategories.map((category: any) => {
										return (
											<option key={category.id} value={category.id}>
												{category.title}
											</option>
										);
									})
								) : null}
							</select>

							<ErrorMessage name="category" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Изображение
							</label>
							<Field className="et0-input-field" type="file" name="image" onChange={handleImageChange} />
							<ErrorMessage name="email" component="div" />
							<img className="et0-img-preload" src={src} alt={alt} />
						</div>

						<button className="et0-button-submit" type="submit" disabled={isSubmitting}>
							Запази
						</button>
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default CategoryForm;
