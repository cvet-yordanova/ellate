export interface IPagination<T> {
    total: number;
    items: T[];
}