import React from 'react';
import PageContainer from '../../../components/page-container';
import AdminHeaderComponent from '../../components/admin-header';
import BackButton from '../../components/back-button';
import ProductForm from '../../components/forms/product-form';
import './admin-product-form-page.styles.scss';


const AdminProductFormPage = () => {
	return (
		<div>
			<AdminHeaderComponent />
			<BackButton/>
			<PageContainer>
				<ProductForm></ProductForm>
			</PageContainer>
		</div>
	);
};

export default AdminProductFormPage;
