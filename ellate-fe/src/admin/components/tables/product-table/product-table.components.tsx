import { useQuery } from '@apollo/client';
import React, { useEffect } from 'react';
import { getAllProductsQuery } from '../../../../graphql/product.query';
import Table from '../../table/table.component';
import { useHistory } from 'react-router-dom';

const ProductTable = () => {


	const history = useHistory();

	const addProductClick = () => {
		history.push('/admin/product/create');
	}

	const { loading, error, data, refetch } = useQuery(getAllProductsQuery, {
		variables: {
			input: {
				productOptions: {},
				searchOptions: { relations: [ 'category' ] }
			}
		}
	});

	useEffect(() => {
		refetch();
	}, []);

	const tableSettings: { columns: any } = {
		columns: {
			id: {
				title: 'Id'
			},
			name: {
				title: 'Име'
			},
			category: {
				title: 'Категория'
			},
			price: {
				title: 'Цена'
			},
			sku: {
				title: 'Sku'
			},
			description: {
				title: 'Описание'
			}
		}
	};

	if (data) {
		console.log(data, 'data');
	}

	if (error) {
		console.log(error, 'error');
	}

	function getMappedData() {
		if (!data) return [];

		return data.allProducts.items.map((product: any) => {
			return { ...product, category: product.category ? product.category.title : '' };
		});
	}

	function onEdit(id: number) {
		history.push('/admin/product/edit/'+id);
	}

	if(loading) {
		return <div>Loading items...</div>
	}

	return (
		<div>
			<div><button onClick={addProductClick}>Add Product</button></div>
			<Table data={getMappedData()} tableSettings={tableSettings} onEdit={onEdit} />
		</div>
	);
};

export default ProductTable;
