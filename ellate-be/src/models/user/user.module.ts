import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Product } from "./../product/product.entity";
import { User } from "./user.entity";
import { UserResolver } from "./user.resolver";
import { UserService } from "./user.service";
import { ProductService } from "./../product/product.service";
import { Basket } from "../basket/basket.entity";

@Module({
    imports: [TypeOrmModule.forFeature([User, Product, Basket])],
    providers: [UserResolver, UserService, ProductService],
    exports: [UserService]
  })
  export class UsersModule {}