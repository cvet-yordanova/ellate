import INotification from '../../models/INotification';
import notificationActionTypes from './notification.types';

export const addNotification = (notification: INotification) => ({
    type: notificationActionTypes.ADD_NOTIFICATION,
    payload: notification
});

export const hideNotification = (notification: INotification) => ({
    type: notificationActionTypes.HIDE_NOTIFICATION,
    payload: notification
});