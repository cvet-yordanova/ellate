import { IUser } from 'src/interfaces/IUser';
import {ManyToMany, JoinTable, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Entity, Index, OneToOne, JoinColumn} from 'typeorm';
import { IsOptional, IsEmail, IsString } from 'class-validator';
import { Product } from '../product/product.entity';
import { Basket } from '../basket/basket.entity';

@Entity()
export class User implements IUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 500})
    firstName: String;

    @Column({length: 500})
    lastName: String;

    @IsEmail()
    @Index({ unique: true })
    @Column({length: 500})
    email: String;

    @Column({length: 500})
    phone: String;

    @Column()
    password: String;

    @OneToOne(() => Basket, (basket) => basket.user)
    basket: Basket;

    @ManyToMany(() => Product)
    @JoinTable()
    favouriteProducts: Product[];

    @IsOptional()
	@Column({ default: false })
    deleted: boolean;

    @CreateDateColumn()
	createdAt?: Date;

	@UpdateDateColumn()
	updatedAt?: Date;
}