import { gql } from "@apollo/client";



export const addProductToBasketMutation = gql`
	mutation AddProductToBasket($input: AddProductToBasket!) {
		addProductToBasket(input: $input) {
			id
            total
            lineItems {
                id
                product {
                    id
                    featuredImage
                    price
                    sku
                    name
                }
                quantity
                total
            }		
		}
	}

`;