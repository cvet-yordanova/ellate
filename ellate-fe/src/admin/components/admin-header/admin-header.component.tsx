import React from 'react';
import { Link } from 'react-router-dom';

import './admin-header.component.scss';

const AdminHeaderComponent = () => {
	return (
		<div className="et0-admin-header">
			<nav>
				<ul>
					<li>
						<Link to="/admin/categories">Categories</Link>
					</li>
                    <li>
						<Link to="/admin/products">Products</Link>
					</li>
				</ul>
			</nav>
		</div>
	);
};

export default AdminHeaderComponent;
