import React from "react";
import PageContainer from "../page-container";
import './footer.styles.scss';
import { Link } from 'react-router-dom';
import { ReactComponent as FacebookIcon } from './../../assets/images/facebook.svg';
import { ReactComponent as InstagramIcon } from './../../assets/images/instagram.svg';


export const Footer = () => (
    <footer className="et0-footer">
        <PageContainer>
            <div className="et0-footer-container">
                <div className="et0-footer-column">
                    <Link className="et0-link" to="#">Често задавани въпроси</Link>
                    <Link className="et0-link" to="#">Условия за ползване на сайта</Link>

                </div>
                <div className="et0-footer-column">
                    <Link className="et0-link" to="#">Връщане и замяна на стока</Link>
                    <Link className="et0-link" to="#">Доставка</Link>
                </div>
                <div className="et0-footer-column social">
                    <Link className="et0-link" to="#">
                        <span className="et-footer-icon facebook"><FacebookIcon/></span>
                    </Link>
                    <Link className="et0-link" to="#">
                        <span className="et-footer-icon instagram"><InstagramIcon/></span>
                    </Link>
                </div>


            </div>
        </PageContainer>
    </footer>


);

export default Footer;