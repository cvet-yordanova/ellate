import { ILineItem } from "./ILineItem";

export interface IBasket {
    id: number;
    total: number;
    lineItems: ILineItem[];
}