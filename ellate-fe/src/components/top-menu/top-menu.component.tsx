import React, { useRef, useState } from 'react';
import './top-menu.styles.scss';
import { Link, useHistory } from 'react-router-dom';
import { ReactComponent as LoupeIcon } from './../../assets/images/loupe.svg';
import { ReactComponent as HeartIcon } from './../../assets/images/heart.svg';
import { ReactComponent as CartIcon } from './../../assets/images/cart-bag.svg';
import { useQuery } from '@apollo/client';
import { findCategoriesQuery } from '../../graphql/category.query';
import { useSelector } from 'react-redux';


const TopMenu = ({ currentUser, hidden }: any) => {
	const [ activeMenuItem, setActiveMenuItem ] = useState<string | null>(null);
	const userState = useSelector((state: any) => state.user);
	const history = useHistory();
    const basketState = useSelector((state: any) => state.basket);

    const getBasketTotal = () => {
        return basketState ? basketState.total : 0;
    }
	
	const { loading, error, data } = useQuery(findCategoriesQuery, {
		variables: { input: { category: null } }
	});

	function updateMenu(id: string) {
		let menuClosed = false;
		if (id === activeMenuItem) {
			menuClosed = true;
		}

		setActiveMenuItem(null);

		if (!menuClosed) {
            setActiveMenuItem(id);
        }
	}

	const getFavouritesCount = () => {
		return userState && userState.favourites && userState.favourites.length || 0;
	}

	const redirectToFavouritesPage = () => {
        history.push("/user/favourites");
    }

	const redirectToBasketPage = () => {
        history.push("/user/basket");
    }

	if (error) {
		console.log(error, 'error');
	}

	const menu1 = useRef<HTMLDivElement>(null);
	const menu2 = useRef<HTMLDivElement>(null);
	const menu3 = useRef<HTMLDivElement>(null);

	const toggleCasualItem = () => {
		if (menu1 && menu1.current) {
			menu1.current.classList.toggle('active');
		}
	};

	const toggleOfficialItem = () => {
		if (menu2 && menu2.current) {
			menu2.current.classList.toggle('active');
		}
	};

	const toggleAccessoriesItem = () => {
		if (menu3 && menu3.current) {
			menu3.current.classList.toggle('active');
		}
	};

	return (
		<div className="et-menu-container">
			<div className="et0-menu-content">
				<div className="et0-menu-items">
					{data && data.findCategories ? (
						data.findCategories.filter((entry: any) => !entry.category).map((category: any) => {
							return (
								<div
									className={`et-menu-item ${category.id === activeMenuItem ? 'active' : ''}`}
									onClick={() => {
										updateMenu(category.id);
									}}
								>
									<div className="et-menu-item-wrapper">
										<div className="et-menu-title">
											<h3>{category.title}</h3>
											<div className="et-menu-item-arrow" />{' '}
										</div>
										<div className="et-menu-list-wrapper">
											<div className="et-menu-list">
												{category.categories.map((entry: any) => {
													return <li className="top-menu-link">
														<Link to={'/category-products/' + entry.id}>
															{entry.title} </Link>
													</li>
												})}
														
											</div>
											<div className="et-menu-img-wrapper">
												<img src={category.image} alt="category image" />
											</div>
										</div>
									</div>
								</div>
							);
						})
					) : null}
				</div>

				<div className="d-flex et0-top-controls">
					<div className="e0-top-search">
						<input className="et0-search-input" />
						<LoupeIcon className="loupe-icon" />
					</div>
					<div className="et0-user-cart-status d-flex">
						<div className="et0-heart-icon-box">
							<span className="et0-count-num">{getFavouritesCount()}</span>
							<HeartIcon className="et0-heart-icon" onClick={redirectToFavouritesPage}/>
						</div>
						<div className="et0-cart-icon-box">
							<span className="et0-count-num">{getBasketTotal()}</span>
							<CartIcon className="et0-cart-icon" onClick={redirectToBasketPage} />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default TopMenu;
