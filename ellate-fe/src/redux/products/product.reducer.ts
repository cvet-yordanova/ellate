import productActionTypes from './product.types';

const INITIAL_STATE = {
	 ids: [], products: {} 
};

const productsReducer = (state = INITIAL_STATE, action: { type: string; payload: any }) => {
	switch (action.type) {
		case productActionTypes.LOAD_PRODUCT_BY_ID:
			return {
				...state,
				products: action.payload
			};
		case productActionTypes.LOAD_PRODUCTS_BY_CATEGORY_SUCCESS:
			return {
				...state,
				products: action.payload
			};
		case productActionTypes.LOAD_PRODUCTS_SUCCESS:
			const newProducts: any[] = action.payload.filter(
				(pr: any) => !state.ids.find((pr: any) => !state.ids.find((e) => e == pr.id))
			);
			const productsState = newProducts.reduce((prev, current) => {
				prev[current.id] = current;
				return prev;
			}, state.products);

			return {
				ids: [ ...state.ids, ...newProducts.map((pr: any) => pr.id) ],
				products: productsState
			};
		default:
			return state;
	}
};

export default productsReducer;
