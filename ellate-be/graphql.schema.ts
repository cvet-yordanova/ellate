
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class AddProductToBasket {
    productId?: string;
}

export class CategoryInput {
    id?: string;
    title?: string;
    slug?: string;
}

export class CreateCategoryInput {
    id?: string;
    title?: string;
    slug?: string;
    image?: Upload;
    category?: CategoryInput;
}

export class FindCategoryInput {
    id?: string;
    title?: string;
    slug?: string;
}

export class FindCategoriesInput {
    title?: string;
    slug?: string;
    category?: FindCategoryInput;
}

export class FindCategoryOptions {
    where?: FindCategoryInput;
    take?: number;
    skip?: number;
    relations?: string[];
}

export class ProductInput {
    id?: string;
    name?: string;
    category?: CategoryInput;
    price?: number;
    enabled?: boolean;
    deleted?: boolean;
    sku?: string;
    slug?: string;
    description?: string;
    featuredImage?: Upload;
}

export class SearchInput {
    page?: number;
    relations?: string[];
    perPage?: number;
}

export class FindProductsInput {
    searchOptions?: SearchInput;
    productOptions?: ProductInput;
}

export class CreateUserInput {
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
    password?: string;
}

export class LoginUserInput {
    email?: string;
    password?: string;
}

export class ProfileObjectInput {
    email?: string;
    familyName?: string;
    givenName?: string;
    googleId?: string;
    imageUrl?: string;
    name?: string;
}

export class TokenObjectInput {
    access_token?: string;
    expires_at?: number;
    expires_in?: number;
    first_issued_at?: number;
    id_token?: string;
    idpId?: string;
    login_hint?: string;
}

export class GoogleLoginInput {
    profileObj?: ProfileObjectInput;
    tokenObj?: TokenObjectInput;
}

export class Asset {
    url?: string;
    name?: string;
}

export class ImageAsset {
    url?: string;
    width?: number;
    height?: number;
}

export class Category {
    id?: string;
    total?: number;
    title?: string;
    slug?: string;
    image?: string;
    category?: Category;
    categories?: Category[];
}

export class Basket {
    id?: string;
    total?: number;
    lineItems?: LineItem[];
}

export class LineItem {
    id?: string;
    product?: Product;
    quantity?: number;
    total?: number;
}

export abstract class IMutation {
    abstract addProductToBasket(input: AddProductToBasket): Basket | Promise<Basket>;

    abstract createCategory(input: CreateCategoryInput): Category | Promise<Category>;

    abstract updateCategory(input: CreateCategoryInput): CategoryUpdateResult | Promise<CategoryUpdateResult>;

    abstract createProduct(input?: ProductInput): Product | Promise<Product>;

    abstract updateProduct(input?: ProductInput): ProductUpdateResult | Promise<ProductUpdateResult>;

    abstract createUser(input: CreateUserInput): User | Promise<User>;

    abstract registerUser(input: CreateUserInput): User | Promise<User>;

    abstract login(input: LoginUserInput): User | Promise<User>;

    abstract addProductToFavourites(input: ProductInput): HttpResponse | Promise<HttpResponse>;

    abstract googleAuthentication(input: GoogleLoginInput): User | Promise<User>;
}

export class ImageFile {
    lastModified?: number;
    name?: string;
    size?: number;
    type?: string;
}

export class CategoryUpdateResult {
    raw?: Product;
    affected?: number;
}

export abstract class IQuery {
    abstract allCategories(): Category[] | Promise<Category[]>;

    abstract findCategories(input?: FindCategoriesInput): Category[] | Promise<Category[]>;

    abstract getCategoryById(id?: string, options?: FindCategoryOptions): Category | Promise<Category>;

    abstract allProducts(input?: FindProductsInput): ProductPaginatedResponse | Promise<ProductPaginatedResponse>;

    abstract getProductById(input?: string): Product | Promise<Product>;

    abstract getProductsByCategoryId(input?: string): Product[] | Promise<Product[]>;

    abstract allUsers(): User[] | Promise<User[]>;

    abstract test(): string | Promise<string>;
}

export class Comment {
    user?: User;
    text?: string;
    date?: Date;
    rating?: number;
}

export class Product {
    id?: string;
    name?: string;
    category?: Category;
    price?: number;
    enabled?: boolean;
    deleted?: boolean;
    slug?: string;
    sku?: string;
    description?: string;
    featuredImage?: string;
}

export class ProductUpdateResult {
    raw?: Product;
    affected?: number;
}

export class ProductPaginatedResponse {
    total?: number;
    items?: Product[];
}

export class User {
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
    password?: string;
    token?: string;
    basket?: Basket;
}

export class HttpResponse {
    code?: number;
    successMessage?: string;
    errMessage?: string;
}

export type Upload = any;
