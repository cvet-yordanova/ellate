import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LineItem } from "../line-item/line-item.entity";
import { Product } from "../product/product.entity";
import { ProductsModule } from "../product/product.module";
import { ProductService } from "../product/product.service";
import { Basket } from "./basket.entity";
import { BasketResolver } from "./basket.resolver";
import { BasketService } from "./basket.service";

@Module({
    imports: [TypeOrmModule.forFeature([Basket, LineItem, Product])],
    providers: [BasketResolver, BasketService, ProductService],
    exports: [BasketService]
  })
export class BasketModule {}