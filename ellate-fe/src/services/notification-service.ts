import { addNotification, hideNotification } from "../redux/notification/notification.actions"
import store from "../redux/store";
import INotification from "../models/INotification";


export const successNotification = (notification: INotification) => {
  store.dispatch(addNotification(notification));

  setTimeout(() => {
      store.dispatch(hideNotification(notification));
  }, 3000);
};