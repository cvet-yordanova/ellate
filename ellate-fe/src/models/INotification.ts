export default interface INotification {
    type: String;
    title: String; 
    message: String;
}