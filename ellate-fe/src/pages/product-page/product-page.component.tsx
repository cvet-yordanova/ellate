import React, { useEffect, useState } from 'react';
import "./product-page.styles.scss";
import fashionWoman from './../../assets/images/fashion-woman.jpg';
import { ReactComponent as StarIcon } from './../../assets/images/star.svg';
import { useDispatch, useSelector } from 'react-redux';
import { loadProductById } from '../../redux/products/product.actions';
import IBaseProps from '../../models/IBaseProps';
import IProduct from '../../models/IProduct';
import PageLayout from '../../components/page-layout';


const ProductPage = (props: IBaseProps) => {

    // const loadedProduct = useSelector();
    const [product, setProduct] = useState<IProduct | null>(null);
    const dispatch = useDispatch();

    const productRes = async (productId: string) => {return await loadProductById(productId, dispatch)};

    useEffect(() => {
        productRes(props.match.params.productId).then(res => {
            setProduct(res.data.getProductById);
        });

    }, []);


    return <div>
        <PageLayout>
            <div className="product-page d-flex">
                <div className="et-image-container">
                    <div className="et-main-image">
                        <img className="" src={fashionWoman} alt="" />
                    </div>
                    <div className="">
                        <div></div>
                    </div>
                </div>
                <div className="et-pr-info-box">
                    <div className="et-pr-title">
                        <h1>{product?.name}</h1>
                    </div>
                    <div className="et-pr-price">
                        <h3>{product?.price}<span className="et0-currency">лв.</span></h3>
                    </div> 
                    <div className="et0-pr-code">
                        <div><p className="et0-text-3">Код : {product?.sku}</p></div>
                        <div className="rating-box">
                            <p className="et0-text-3">Оцени продукта</p>
                            <div className="et0-rating-box">
                                <StarIcon className="et0-star-icon"/>
                                <StarIcon className="et0-star-icon"/>
                                <StarIcon className="et0-star-icon"/>
                                <StarIcon className="et0-star-icon"/>
                                <StarIcon className="et0-star-icon"/>
                            </div>
                        </div>
                    </div>
                    <div className="et0-color-box">
                        <div>
                            <p className="et0-text-3">Избери цвят: бежов</p>
                        </div>
                        <ul>
                            <li className="et0-pr-color"></li>
                            <li className="et0-pr-color"></li>
                            <li className="et0-pr-color"></li>
                            <li className="et0-pr-color"></li>
                        </ul>
                    </div>
                    <div className="et0-buy-btn">
                        <input className="et-btn-1" type="button" value="Добави в кошницата" />
                    </div>
                    {/* //tstodo partially visible */}
                    <div className="et0-description">
                        <p className="et0-text-3">
                            {product?.description}
                        </p>
                    </div>
                    <div />
                </div>
            </div>
        </PageLayout>
    </div>


}


export default ProductPage;