const notificationActionTypes = {
    ADD_NOTIFICATION: 'ADD_NOTIFICATION',
    HIDE_NOTIFICATION: 'HIDE_NOTIFICATION',
    HIDE_ALL_NOTIFICATION: 'HIDE_ALL_NOTIFICATION'
}

export default notificationActionTypes;