import React from 'react';
import './notification.styles.scss';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import IReducer from './../../models/IReducer';
import INotification from '../../models/INotification';

const Notification = () => {
	const notifications = useSelector((state: IReducer) => state.notification.notifications);

	return notifications && notifications.length ? (
		<div>
			{notifications.map((n: INotification) => {
				return <div>
					<div className="et0-notification-box">
						<h5>{n.title}</h5>
						<p>{n.message}</p>
					</div>
				</div>;
			})}
		</div>
	) : null;
};

export default Notification;
