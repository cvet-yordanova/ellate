import { Query, Resolver, Mutation, Args } from "@nestjs/graphql";
import { Category } from "./category.entity";
import { CategoryService } from "./category.service";
import { createWriteStream } from "fs";
import { FindOneOptions, UpdateResult } from "typeorm";
const path = require("path");

@Resolver('Category')
export class CategoryResolver {

  constructor(private categoryService: CategoryService) { }

  @Query('allCategories')
  async allUsers(): Promise<Category[]> {
    return this.categoryService.getAll();
  }

  @Query('getCategoryById')
  async getCategoryById(id: string | number, findOptions: FindOneOptions<Category>): Promise<Category> {
    return this.categoryService.findById(id, findOptions);
  }

  @Query('findCategories')
  async findCategories(@Args() args: {input: any}): Promise<Category[]> {
    return this.categoryService.findCategories(args.input);
  }

  @Mutation('createCategory')
  async createCategory(@Args() args: any): Promise<Category> {
    const { createReadStream, filename } = await args.input.image;

    const savePath = path.join(__dirname,'../../../../product_images/', filename);
   
    await new Promise(res =>
      createReadStream() 
        .pipe(createWriteStream(savePath))
        .on("close", res)
        .on("error", (error) => {
          console.error(error, 'error')
        })
    );

    delete args.input.image

    return this.categoryService.create({...args.input, image: savePath});
  }

  @Mutation('updateCategory')
  async updateCategory(@Args() args: any): Promise<UpdateResult> {
    const {input} = args;
    return await this.categoryService.update(input.id, input);
  }

}