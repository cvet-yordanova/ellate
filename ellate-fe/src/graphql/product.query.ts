import { gql } from '@apollo/client';

export const getAllProductsQuery = gql`
	query GetAllProductsQuery($input: FindProductsInput) {
		allProducts(input: $input) {
			total
			items {
				id
				name
				category {
					id
					title
				}
				price
				enabled
				deleted
				sku
				description
				featuredImage
			}
		}
	}
`;


export const getProductsByCategoryIdQuery = gql`
	query GetProductsByCategoryIdQuery($input: String) {
		getProductsByCategoryId(input: $input) {
			
				id
				name
				category {
					id
					title
				}
				price
				enabled
				sku
				description
				featuredImage
			
		}
	}

`;

export const getProductByIdQuery = gql`
	query GetProductById($input: String) {
		getProductById(input: $input) {
			id
			name
			category {
				id
				title
			}
			price
			enabled
			deleted
			sku
			description
			featuredImage
		}
	}
`;

export const createProductMutation = gql`
	mutation CreateProduct($input: ProductInput!) {
		createProduct(input: $input) {
			id
			name
			description
		}
	}
`;

export const updateProductMutation = gql`
	mutation UpdateProduct($input: ProductInput!) {
		updateProduct(input: $input) {
			affected
		}
	}
`
