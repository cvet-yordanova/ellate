import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { UsersModule } from './models/user/user.module';
import { CategoryModule } from './models/category/category.module';
import { ProductsModule } from './models/product/product.module';
import { ConfigModule } from '@nestjs/config';
import { BasketModule } from './models/basket/basket.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
    }),
    TypeOrmModule.forRoot({
      type: process.env.DB_TYPE,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      autoLoadEntities: true,
      synchronize: true,
    } as TypeOrmModuleOptions | any),
    UsersModule,
    ProductsModule,
    CategoryModule,   
    BasketModule
  ],
})
export class AppModule {}
