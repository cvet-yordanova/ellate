import { IProduct } from "./IProduct";

export interface ILineItem {
    id: number;
    product: IProduct;
    quantity: number;
    total: number;
}