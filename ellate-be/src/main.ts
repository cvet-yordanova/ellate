import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
  app.enableCors();
  console.log('App listening on port 3000');
  console.log('Graphql playground is listening on http://localhost:3000/graphql');
}
bootstrap();
