import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const congif = {
    apiKey: "AIzaSyBP2epR9bzwXuXsRQPjRiFHNA0YZ5POh5g",
    authDomain: "ellate.firebaseapp.com",
    databaseURL: "https://ellate.firebaseio.com",
    projectId: "ellate",
    storageBucket: "ellate.appspot.com",
    messagingSenderId: "678230836550",
    appId: "1:678230836550:web:2a637cd17c45188c9a5147",
    measurementId: "G-14H7VJQMFY"
}

firebase.initializeApp(congif);

export const auth = firebase.auth();
export const firestore = firebase.firestore();


const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});


export const signInWithGoogle = () => auth.signInWithPopup(provider);

export const createUserProfileDocument = async (userAuth: any, additionalData?: any) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapshot = await userRef.get();

    if(!snapshot.exists) {
        const {displayName, email} = userAuth;
        const createdAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch(err) {
            console.log('error creating user', err.message);
        }

    }

    return userRef;

}

export default firebase;