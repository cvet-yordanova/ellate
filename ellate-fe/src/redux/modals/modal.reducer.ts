import modalActionTypes from './modal.types';

const INITIAL_STATE = {
	loginModal: { visible: false }
};

const modalReducer = (state = INITIAL_STATE, action: { type: string; payload: any }) => {
	switch (action.type) {
		case modalActionTypes.SHOW_LOGIN_MODAL:
			return {
				...state,
				loginModal: { visible: true }
			};

		case modalActionTypes.HIDE_LOGIN_MODAL:
			return {
				...state,
				loginModal: { visible: false }
			};

		case modalActionTypes.CLOSE_MODAL:
			return {
				...state,
				[action.payload]: { visible: false }
			};

		default:
			return state;
	}
};

export default modalReducer;
