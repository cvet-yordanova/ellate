import INotification from '../../models/INotification';
import notificationActionTypes from './notification.types';


const INITIAL_STATE = {
	notifications:<INotification[]>[]
};

const modalReducer = (state = INITIAL_STATE, action: { type: string; payload: INotification }) => {
	switch (action.type) {
		case notificationActionTypes.ADD_NOTIFICATION:
            
			return {
				...state,
				notifications: [...state.notifications, action.payload]
			};

		case notificationActionTypes.HIDE_NOTIFICATION:
            let idx = state.notifications.indexOf(action.payload);
			state.notifications.splice(idx, 1);
			return {
				...state,
				notifications: [...state.notifications]
			};
		default:
			return state;
	}
};

export default modalReducer;
