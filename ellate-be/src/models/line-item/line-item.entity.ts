import { IsNumber, Min } from "class-validator";
import { ILineItem } from "src/interfaces/ILineItem";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Basket } from "../basket/basket.entity";
import { Product } from "../product/product.entity";

@Entity()
export class LineItem implements ILineItem {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Product, {eager: true})
    @JoinColumn()
    product: Product;

    @ManyToOne(() => Basket, (basket) => basket.lineItems)
    @JoinColumn()
    basket: Basket;

    @IsNumber()
    @Column({default: 0 })
    @Min(0)
    quantity: number;

    @IsNumber()
    @Column({default: 0 })
    @Min(0)
    total: number;
}