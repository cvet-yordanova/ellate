import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Category } from "../category/category.entity";
import { Product } from "./product.entity";
import { ProductResolver } from "./product.resolver";
import { ProductService } from "./product.service";

@Module({
    imports: [TypeOrmModule.forFeature([Product, Category])],
    providers: [ProductResolver, ProductService],
    exports: [ProductService]
  })
  export class ProductsModule {} 