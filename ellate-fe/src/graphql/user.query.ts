import {gql} from '@apollo/client';


export const registerUserMutation = gql`
    mutation RegisterUser($input: CreateUserInput!) {
        createUser(input: $input) {
            id
            firstName
            lastName
            email
            basket {
                total
            }
        }
    }

`
export const loginUserMutation = gql`
    mutation LoginUser($input: LoginUserInput!) {
        login(input: $input) {
        email
        firstName
        lastName
        basket {
            total
        }
        token
    }
  }
`


export const addProductToFavouritesMutation = gql`
    mutation AddProductToFavouritesMutation($input: ProductInput!) {
        addProductToFavourites(input: $input) {
        code
        successMessage
        errMessage
    }
  }
`
