import React from 'react';
import { useSelector } from 'react-redux';
import Table from '../../admin/components/table';
import PageLayout from '../../components/page-layout';


const FavouritesPage = (props: any) => {

    const userState = useSelector((state: any) => state.user);
    const productsState = useSelector((state: any) => state.shopping);

    const getTableSettings = () => {
        return {
            columns: {
                image: {title: 'Изображение'},
                name: {title: 'Артикул'},
                price: {title: 'Цена'},
                sku: {title: 'Sku'},
            }
        }
    }

    const getFavouriteProducts = () => {

        const favourites = userState && userState.favourites;
        
         if (favourites && productsState && productsState.products) {
            return favourites.filter((productId: any) => {
                        return productsState.ids.includes(productId);
                    }).map((prId: any) => {
                        return productsState.products[prId];
                    });
        } else return []

    };


    return(
        <PageLayout>
            <Table data={getFavouriteProducts()} tableSettings={getTableSettings()} onEdit={() => {}}></Table>
        </PageLayout>
    )

}

export default FavouritesPage;