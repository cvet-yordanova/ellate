
import IModalReducer from './IModalReducer';
import INotificationReducer from './INotificationReducer';

export default interface IReducer {
	user: any;
	cart: any;
	modal: IModalReducer;
	notification: INotificationReducer
};
