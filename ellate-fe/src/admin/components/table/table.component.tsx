import React from 'react';
import './table.styles.scss';

const Table = (props: any) => {

    if(!props.data) return <div>There are no entries.</div>

    if(!props.tableSettings) return <div></div>;

	//tstodo
	console.log(props.data, 'props data')

	return (
		<div>
			<table className="et0-table">
				<thead>
					<tr className="et0-table-heading">
						{Object.keys(props.tableSettings.columns).map((tableColumn: string, headIdx: number) => {
							return <th key={`head-${headIdx}`} >{props.tableSettings.columns[tableColumn].title}</th>;
						})}
					</tr>
				</thead>
				<tbody>
					{props.data.map((entry: any, rowIdx: number) => {
						return (
							<tr key={`row-${rowIdx}`}>
								{Object.keys(props.tableSettings.columns).map((tableColumn: string, dataIdx: number) => {
									return <td key={`data-${dataIdx}`}>{entry[tableColumn]}</td>;
								})}
								<td><span onClick={() => {props.onEdit(entry.id)}}>edit</span></td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default Table;
