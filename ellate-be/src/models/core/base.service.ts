import { DeepPartial, Repository } from 'typeorm';

export abstract class BaseService<T> {
  constructor(protected repository: Repository<T>) {}

  async getAll() {
    return this.repository.find();
  }

  async getById(id: string | number): Promise<any> {
    return this.repository.findOne(id);
  }

  async create(entity: DeepPartial<T>): Promise<any> {
    const newEntity = this.repository.create(entity);

    return await this.repository.save(newEntity);
  }

  async update(id: string | number, entity: DeepPartial<T> ) {

    return await this.repository.update(id, entity);
  }

  async delete(id: number): Promise<any> {
    return await this.repository.delete(id);
  }
}
