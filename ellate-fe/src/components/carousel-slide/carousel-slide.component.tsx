import React from 'react';
import './carousel-slide.styles.scss';

const CarouselSlide = (props: { imageUrl: string }) => {
	return (
		<div className="et0-slide-container">
			<div className="et0-slide-content">
				<img src={props.imageUrl} className="et0-slide-image" />
				<div className="et0-slide-overlay-data">
					<div className="at0-slide-text">
						<h2>Lorem, ipsum dolor.</h2>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. In animi consequuntur optio
							placeat numquam tenetur?
						</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CarouselSlide;
