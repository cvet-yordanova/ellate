export default interface INotificationReducer {
	notifications: {type: String, title: String, message: String}[];
}