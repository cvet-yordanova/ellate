import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import './login-modal.styles.scss';
import { useSelector } from 'react-redux';
import {ReactComponent as CloseIcon} from './../../../assets/images/cancel.svg';
import { useDispatch } from 'react-redux'

import IReducer from './../../../models/IReducer';
import { closeModal } from '../../../redux/modals/modal.actions';
import MODAL_TYPES from '../../../models/IModalTypes';
import { login } from '../../../redux/user/user.actions';
import GoogleLogin from 'react-google-login';

const LoginModal = () => {
	const modalVisible = useSelector((state: IReducer) => state.modal.loginModal.visible);
	const dispatch = useDispatch();

	console.log(process.env.REACT_APP_GOOGLE_CLIENT_ID, 'env')


	const handleLogin = (res: any) => {
		//tstodo
		console.log(res, 'res from login')
	}

	return (
		modalVisible ? <div className="et0-overlay">
			<div className="et0-form et0-login-form">
				<CloseIcon onClick={() => {dispatch(closeModal(MODAL_TYPES.loginModal))}} className="et0-modal-close-icon"/>

				<h1 className="et0-form-label">Вход</h1>
				<Formik
					initialValues={{ email: '', password: ''}}
					onSubmit={(values, { setSubmitting }) => {

						login(values, dispatch).then(res => {
							if(res) {
								dispatch(closeModal(MODAL_TYPES.loginModal))
							}
						});

						setTimeout(() => {
							setSubmitting(false);
						}, 400);
					}}
				>
					{({ isSubmitting }) => (
						<Form>
							<div className="et0-form-field">
								<label className="et0-field-label" htmlFor="">
									Имейл
								</label>
								<Field className="et0-input-field" type="email" name="email" />
								<ErrorMessage name="email" component="div" />
							</div>

							<div className="et0-form-field">
								<label className="et0-field-label" htmlFor="">
									Парола
								</label>
								<Field className="et0-input-field" type="password" name="password" />
								<ErrorMessage name="password" component="div" />
							</div>

							<button className="et0-button-submit full-width" type="submit" disabled={isSubmitting}>
								Submit
							</button>
						</Form>
					)}
				</Formik>

				<GoogleLogin
					clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID as any}
					buttonText="Log in with Google"
					onSuccess={handleLogin}
					onFailure={handleLogin}
					cookiePolicy={'single_host_origin'}
				/>
			</div>
		</div> : null
	);
};

export default LoginModal;
