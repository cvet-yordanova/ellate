import React from "react";
import SHOP_DATA from "./shop-data";
import CollectionPreview from "../../components/collection-preview/collection-preview.component";

// MyProps, MyState
class ShopPage extends React.Component<{}, { collections: any }> {

    constructor(props: Props) {
        super(props);

        this.state= {
            collections: SHOP_DATA
        }
    }

    render() {
        const { collections } = this.state;
        return <div className="shop-page">
            {collections.map(({ id, ...otherCollectionProps }: any) => {
                return <CollectionPreview key={id} {...otherCollectionProps} />
            })}
        </div>
    }
}

type Props = {

}

export default ShopPage;