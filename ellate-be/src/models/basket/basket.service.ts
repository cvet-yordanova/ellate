import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { verifyToken } from "src/helpers/user.helper";
import { FindOneOptions, Repository } from "typeorm";
import { BaseService } from "../core/base.service";
import { LineItem } from "../line-item/line-item.entity";
import { ProductService } from "../product/product.service";
import { Basket } from "./basket.entity";

@Injectable()
export class BasketService extends BaseService<Basket> {
	constructor(@InjectRepository(Basket) protected basketRepository: Repository<Basket>,
				@InjectRepository(Basket) protected lineItemRepository: Repository<LineItem>,
										  private productService: ProductService) {
		super(basketRepository);
	}


	async addProductToBasket(productId, context) {

		const decoded = verifyToken(context);
		const basket = await this.basketRepository.findOne({
			where: {"user": {id: decoded.user_id }},
			relations: ["user", "lineItems", "lineItems.product"]});

		const product = await this.productService.findProductById(productId, {});

		if(!product) {
			throw new HttpException("product.not.found", HttpStatus.NOT_FOUND);
		}

		const lineItem = (Object.assign(new LineItem(), {product, quantity: 0}))

		basket.lineItems.push(lineItem);

		await this.basketRepository.save(basket);

		return {lineItems: basket.lineItems, total: basket.total}
	}

}