import { gql } from '@apollo/client';

export const getAllCategoriesQuery = gql`
	query getAllCategories {
		allCategories {
			id,
			title
			slug
			image
		}
	}
`;

export const getCategoryByIdQuery = gql`
	query GetCategoryById($id: String, $options: FindCategoryOptions) {
		getCategoryById(id: $id, options: $options) {
			id
			title
			slug
			image
			category {id, title}
			categories {id, title}
		}
	}
`;

export const findCategoriesQuery = gql`
	query GetFilteredCategories($input: FindCategoriesInput) {
		findCategories(input: $input) {
			id
			title
			slug
			image
			category {id, title}
			categories {id, title}
		}
	}
`;

export const createCategoryQuery = gql`
	mutation CreateCategory($input: CreateCategoryInput!) {
		createCategory(input: $input) {
			title
			slug
			image
		}
	}
`;

export const updateCategoryMutation = gql`
	mutation UpdateCategory($input: CreateCategoryInput!) {
		updateCategory(input: $input) {
			affected
		}
	}

`;
