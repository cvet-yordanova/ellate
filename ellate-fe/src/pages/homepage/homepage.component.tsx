import React from 'react';
import './homepage.styles.scss';

import PageContainer from '../../components/page-container';
import CardRow from '../../components/card-row';
import Footer from '../../components/footer';
import TopMenu from '../../components/top-menu';
import Header from '../../components/header';
import CarouselSlidesComponent from '../../components/carousel-slides';
import LoginModal from '../../components/modals/login-modal';

const HomePage = () => {
	return (
		<div>
			<div>
				<LoginModal />
			</div>
			<Header />
			<TopMenu />
			<CarouselSlidesComponent />
			<PageContainer>
				<CardRow />
				<CardRow />
				<CardRow />
			</PageContainer>
			<Footer />
		</div>
	);
};

export default HomePage;
