import { addProductToBasket } from "../../services/basket.service";
import BasketActionTypes from "./basket.types";


export const addProductToUserBasket = (productId: any, dispatchFunction: Function) => {

      dispatchFunction({type: BasketActionTypes.ADD_PRODUCT_TO_BASKET});

      return addProductToBasket(productId).then((res: any) => {
          console.log(res, 'res');


         dispatchFunction({type: BasketActionTypes.UPDATE_BASKET_SUCCESS,
                            payload: {basket: res.data.addProductToBasket}});

      });



};