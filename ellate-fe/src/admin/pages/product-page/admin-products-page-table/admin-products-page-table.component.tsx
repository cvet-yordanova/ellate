import React from 'react';
import PageContainer from '../../../../components/page-container';
import AdminHeaderComponent from '../../../components/admin-header';
import ProductTable from '../../../components/tables/product-table';


const AdminProductsTablePage = () => {
    return (
		<div>
			<AdminHeaderComponent/>
			<PageContainer>
				< ProductTable/>
			</PageContainer>
		</div>
	);
}

export default AdminProductsTablePage;