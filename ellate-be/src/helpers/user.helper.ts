import { HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

export const verifyToken = (context) => {

    const headers = context.req.headers;
    const authorization = headers.authorization;

    const token = authorization ? authorization.split(" ")[1] : null;

    if (!token) {
        throw new HttpException("token.not.provided", HttpStatus.BAD_REQUEST);
    }

    let decoded;

    try {
      decoded = jwt.verify(token, process.env.TOKEN_KEY);
    } catch (err) {
      throw new HttpException("token.not.valid", HttpStatus.BAD_REQUEST);
    }
    return decoded;
};