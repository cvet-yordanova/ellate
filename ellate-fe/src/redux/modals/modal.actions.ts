import modalActionTypes from './modal.types';

export const showLoginModal = () => ({
	type: modalActionTypes.SHOW_LOGIN_MODAL
});

export const hideLoginModal = () => ({
	type: modalActionTypes.HIDE_LOGIN_MODAL
});

export const closeModal = (modal: String) => ({
    type: modalActionTypes.CLOSE_MODAL,
    payload: modal
});