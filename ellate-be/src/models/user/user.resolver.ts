import { Query, Resolver, Mutation, Args, Context } from "@nestjs/graphql";
import { UserService } from "./user.service";
import {User} from "./user.entity";
 
@Resolver('User')
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query('test')
  async test(): Promise<string> {
    return 'test';
  }

  @Query('allUsers')
  async allUsers(): Promise<any[]> {
    return this.userService.getAll();
  }

  @Mutation('createUser')
  async createUser(@Args() args: any): Promise<User> {
    return this.userService.createUser(args.input);
  }

  @Mutation('login')
  async login(@Args() args: any, @Context() context): Promise<any> {
    return this.userService.login(args.input);
  }

  @Mutation('addProductToFavourites')
  async addProductToFavourites(@Args() args: any, @Context() context: any) {
    const input = args.input;
    return this.userService.addProductToFavourites(input.id, context)
  }

  @Mutation('googleAuthentication')
  async googleAuthentication(@Args() args: any, @Context() context: any) {
    const input = args.input;
    //tstodo
    console.log(input, "input")
    //return this.userService.addProductToFavourites(input.id, context)
  }

}