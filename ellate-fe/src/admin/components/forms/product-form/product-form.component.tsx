import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

import './product-form.styles.scss';
import { useQuery } from '@apollo/client';
import { getAllCategoriesQuery } from '../../../../graphql/category.query';
import { createProduct } from '../../../../services/product.service';
import { useHistory, useParams } from 'react-router-dom';
import { getProductByIdQuery } from '../../../../graphql/product.query';
import { updateProduct } from '../../../../services/product.service';
import { successNotification } from '../../../../services/notification-service';

const ProductForm = () => {
	const history = useHistory();
	const [ file, updateFile ] = useState(null);
	const { loading, error, data } = useQuery(getAllCategoriesQuery);
	let { id } = useParams<{ id?: string }>();

	const { loading: productLoading, error: getProductErr, data: product } = useQuery(getProductByIdQuery, {
		variables: { input: id }
	});

	const [ { alt, src }, setImg ] = useState({
		src: 'https://i.stack.imgur.com/y9DpT.jpg',
		alt: 'Upload an Image'
	});

	function handleImageChange(event: any) {
		const { target: { value, files } } = event;
		const fileForUpload = files[0];

		if (fileForUpload) {
			setImg({
				src: URL.createObjectURL(fileForUpload),
				alt: fileForUpload.name
			});
		}

		updateFile(fileForUpload || value);
	}

	if (productLoading) {
		return <div>Loading Product ...</div>;
	}

	let { getProductById } = product && id ? product : { getProductById: null };

	return (
		<div className="et0-form et0-admin-product-form">
			<h1 className="et0-form-label">Артикул</h1>
			<Formik
				initialValues={{
					name: getProductById ? getProductById.name : '',
					price: getProductById ? getProductById.price : '',
					slug: getProductById ? getProductById.slug : '',
					sku: getProductById ? getProductById.sku : '',
					enabled: getProductById && getProductById.enabled ? [ 'on' ] : '',
					description: getProductById ? getProductById.description : '',
					category: getProductById && getProductById.category ? getProductById.category.id : '',
					featuredImage: getProductById ? getProductById.featuredImage : ''
				}}
				validate={(values) => {
					const errors: any = {};
					//tstodo
					return errors;
				}}
				onSubmit={(values, { setSubmitting }) => {
					if (getProductById && getProductById.id) {
						updateProduct({
							...values,
							id: getProductById.id,
							enabled: values.enabled.includes('on'),
							category: { id: values.category }
						})
							.then((res: any) => {
								const notification = { message: 'Product updated', title: 'Success', type: 'Success' };
								successNotification(notification);

								history.push('/admin/products');
							})
							.catch((err: any) => {
								history.push('/admin/products');
							});
					} else {
						createProduct({
							...values,
							enabled: values.enabled.includes('on'),
							category: { id: values.category }
						})
							.then((res) => {
								const notification = { message: 'Product created', title: 'Success', type: 'Success' };
								successNotification(notification);

								history.push('/admin/products');
							})
							.catch((err) => {
								history.push('/admin/products');
							});
					}

					setSubmitting(false);
				}}
			>
				{({ isSubmitting, handleChange, handleBlur, values }) => (
					<Form>
						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Име
							</label>
							<Field className="et0-input-field" type="text" name="name" />
							<ErrorMessage name="name" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Цена
							</label>
							<Field className="et0-input-field" type="number" name="price" />
							<ErrorMessage name="price" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Slug
							</label>
							<Field className="et0-input-field" type="text" name="slug" />
							<ErrorMessage name="slug" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Sku
							</label>
							<Field className="et0-input-field" type="text" name="sku" />
							<ErrorMessage name="sku" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Активен
							</label>
							<input
								className="et0-input-checkbox"
								type="checkbox"
								name="enabled"
								checked={values.enabled.includes('on')}
								onChange={handleChange}
							/>
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Описание
							</label>
							<textarea
								className="et0-input-field"
								rows={5}
								name="description"
								value={values.description}
								onChange={handleChange}
							/>
							<ErrorMessage name="parent" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Категория
							</label>
							<select
								name="category"
								value={values.category}
								onChange={handleChange}
								style={{ display: 'block' }}
								className="et0-input-field"
							>
								<option value="null">-</option>
								{data && data.allCategories ? (
									data.allCategories.map((category: any) => {
										return (
											<option key={category.id} value={category.id}>
												{category.title}
											</option>
										);
									})
								) : null}
							</select>

							<ErrorMessage name="category" component="div" />
						</div>

						<div className="et0-form-field">
							<label className="et0-field-label" htmlFor="">
								Изображение
							</label>
							<Field
								className="et0-input-field"
								type="file"
								name="featuredImage"
								onChange={handleImageChange}
							/>
							<ErrorMessage name="email" component="div" />
							<img className="et0-img-preload" src={src} alt={alt} />
						</div>

						<button className="et0-button-submit" type="submit" disabled={isSubmitting}>
							Запази
						</button>
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default ProductForm;
