import React from 'react';
import './App.css';
import HomePage from './pages/homepage/homepage.component';
import ShopPage from './pages/shop/shop.component'
import ProductPage from './pages/product-page/product-page.component'
import { Switch, Route } from 'react-router-dom';
import SignInAndSignUpPage from "./pages/signin-and-signup/signin-and-signup.component";
import { auth } from "./firebase/firebase.utils";
import { createUserProfileDocument } from "./firebase/firebase.utils";
import RegistrationPage from './pages/registration';
import { testFunction } from './services/auth.service';
import { ApolloProvider } from "@apollo/client";
import {apolloClient} from './'
import AdminProductFormPage from './admin/pages/product-page';
import Notification from './components/notification';
import AdminProductsTablePage from './admin/pages/product-page/admin-products-page-table';
import AdminCategoriesTablePage from './admin/pages/category-page/admin-categories-table-page';
import AdminCategoryFormPage from './admin/pages/category-page';
import CategoryPage from './pages/category-page/category-page.component';
import FavouritesPage from './pages/favourites';
import BasketPage from './pages/basket';


class App extends React.Component {


  constructor() {
    super({});

    this.state = {
      currentUser: null
    };
  }

  unsubscribeFromAuth: any = null;

  componentDidMount() {

    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef: any = await createUserProfileDocument(userAuth);

        userRef.onSnapshot((snapShot: any) => {
          this.setState({
            currentUser: {
              id: snapShot.id,
              ...snapShot.data()
            }
          });
        });
      }
    });

    testFunction(); 
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }

  render() {
    return<ApolloProvider client={apolloClient}> <div>
          <Notification/>
          <Switch>
            <Route exact path="/" component={HomePage} />
            {/* <Route exact path="/details" component={ProductPage} /> */}
            <Route path="/shop" component={ShopPage} />
            <Route path="/category-products/:categoryId" component={CategoryPage} />
            <Route path="/signin" component={SignInAndSignUpPage} />
            <Route path="/registration" component={RegistrationPage} />
            <Route path="/products/:productId" component={ProductPage}/>
            <Route path="/user/favourites" component={FavouritesPage}/>
            <Route path="/user/basket" component={BasketPage}/>
            <Route path="/admin/products" component={AdminProductsTablePage}/>
            <Route path="/admin/categories" component={AdminCategoriesTablePage}/>
            <Route path="/admin/category/create" component={AdminCategoryFormPage}/>
            <Route path="/admin/category/edit/:id" component={AdminCategoryFormPage}/>
            <Route path="/admin/product/create" component={AdminProductFormPage}/>
            <Route path="/admin/product/edit/:id" component={AdminProductFormPage}/>
          </Switch>
        </div></ApolloProvider>
  };
}

export default App;
