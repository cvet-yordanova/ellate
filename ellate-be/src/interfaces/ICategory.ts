import { IProduct } from "./IProduct";

export interface ICategory {
    id?: number;
    title: String;
    slug: String;
    image: String;
    category?: ICategory;
    categories?: ICategory[],
    products?: IProduct[];

    //tstodo
    // parent
}