import categoryPageActionTypes from "./category-page.types";

export interface ICategoryPage {
    lastLoadedPage?: number,
    categoryId: any,
    productIds: any[]
};

export interface ICategoryPageState {
    productsPerPage: number,
    categories: any
}

const INITIAL_STATE: ICategoryPageState = {
    productsPerPage: 12,
    categories: {}
};


const productsReducer = (state = INITIAL_STATE, action: {type: string, payload: ICategoryPage}) => {

    if(!action.payload) return state;
    const loadedCategory = state.categories[action.payload.categoryId];
    let categoryState: any = {};

    if (loadedCategory && action.payload) {
        categoryState = {...loadedCategory, 
            lastLoadedPage: loadedCategory.lastLoadedPage ? loadedCategory.lastLoadedPage++ : 1,
            productIds: [...loadedCategory.productIds, ...action.payload.productIds]
        }
    } else if(action.payload) {
        categoryState = {
            lastLoadedPage: 1,
            categoryId: action.payload?.categoryId,
            productIds: action.payload?.productIds
        }
    }

    switch(action.type) {
        case categoryPageActionTypes.LOAD_PRODUCTS_FOR_CATEGORY_PAGE:
            return {
                categories: {...state.categories,[categoryState.categoryId]: categoryState}
            };
        default: return state;
    }
}

export default productsReducer;