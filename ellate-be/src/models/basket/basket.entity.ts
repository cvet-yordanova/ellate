import { IsNumber, Min } from "class-validator";
import { IBasket } from "src/interfaces/IBasket";
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { LineItem } from "../line-item/line-item.entity";
import { User } from "../user/user.entity";


@Entity()
export class Basket implements IBasket {
    @PrimaryGeneratedColumn()
    id: number;

    @IsNumber()
    @Column({default: 0 })
    @Min(0)
    total: number;

    @OneToMany(() => LineItem, (item) => item.basket, {cascade: true})
    lineItems: LineItem[];

    @OneToOne(() => User, (user) => user.basket)
    @JoinColumn()
    user: User;
    
}