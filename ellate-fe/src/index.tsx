import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createUploadLink } from 'apollo-upload-client';

import store from './redux/store';

import './index.scss';
import App from './App';

import { ApolloClient, InMemoryCache, HttpLink, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: 'http://localhost:3000/graphql',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});


export const apolloClient = new ApolloClient({
  // uri: 'http://localhost:3000/graphql',
  //@ts-ignore
  link: authLink.concat(httpLink).concat(createUploadLink({
    uri: 'http://localhost:3000/graphql'
  })),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);




//tstodo
//x search
// search working
//x register form
//-register form working
//-responsive cards row
//-responsive details
//x slider banner
//-xfooter
//-categories json
//-notification redux
//-notification teplate
//-checkout page
//-favourite products list
//-add to favourites
//-add to cart
//-filter
//-arrows on top menu
// -scroll items with arrows
// -to top
// -back button
// -global validators
// -admin sidebar
// -admin login
// -add image to category
// -loading
// -admin login
