import { ICategory } from 'src/interfaces/ICategory';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IsString } from 'class-validator';
import { Product } from '../product/product.entity';
@Entity()
export class Category implements ICategory {
	@PrimaryGeneratedColumn() id: number;

	@Column({ length: 500 })
	title: string;

	@Column() slug: string;

	/**
	 * parent category
	 */
	@IsString()
	@ManyToOne(type => Category, childCategory => childCategory.categories)
	@JoinColumn()
	category?: Category;

	@IsString()
	@Column({ nullable: true })
	image: string;

	/**
	 * child categories
	 */
	@OneToMany(type => Category, category => category.category)
	categories: ICategory[];

	@OneToMany(() => Product, product => product.category)
	products: Product[];
}
