import React from "react";

import "./card.styles.scss";
import { ReactComponent as CartIcon } from "../../assets/images/cart-bag.svg";
import { ReactComponent as HeartIcon } from "../../assets/images/heart.svg";

import pic1 from '../../assets/images/pic1.jpg';
import pic2 from '../../assets/images/pic2.jpg';
import { useHistory } from "react-router-dom";
import { addToFavourites } from "../../redux/user/user.actions";
import { useDispatch, useSelector } from "react-redux";
import { addProductToUserBasket } from "../../redux/basket/basket.actions";

export interface CardProps {
    productData?: any
}


const Card = (props: CardProps) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const userState = useSelector((state: any) => state.user);

    const redirectToPage = () => {
        history.push("/products/"+props.productData.id);
    }

    const isFavourite = (productId: any) => {
        return userState && userState.favourites.includes(productId);
    }

    const addToFavs = (e: any) => {
        e.stopPropagation();
        addToFavourites(props.productData?.id, dispatch);
    }

    const addToBasket = (e: any) => {
        e.stopPropagation();
        addProductToUserBasket(props.productData?.id, dispatch);
    }
    

    return <div className="et0-card" onClick={redirectToPage}>
        <div className="et0-card-shape">
            <div className="et0-card-content">
                <div className="et0-featured-image">
                    <div className="et0-image-box">
                        <div className="et0-image1">
                            <img src={pic1} alt="" />
                        </div>
                        <div className="et0-image2">
                            <img src={pic2} alt="" />
                        </div>
                    </div>
                    <div>
                        <span className="et0-like">
                            <HeartIcon className={`${isFavourite(props.productData?.id) ? 'active' : ''} et0-card-heart` } onClick={addToFavs}/></span>
                    </div>
                </div>
                <div className="et0-description-box">
                    <div className="et0-pr-title">
                        <h1>{props.productData?.name}</h1>
                    </div>
                    <div className="et0-cart-price">
                        <div>
                            <p className="et0-cart-price-text">{props.productData?.price}<span>лв.</span></p>
                        </div>
                        <div className="et0-cart-icon-wrap">
                            <CartIcon className="et0-cart-icon" onClick={addToBasket}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default Card;