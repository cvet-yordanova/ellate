import { useQuery } from '@apollo/client';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { findCategoriesQuery } from '../../../../graphql/category.query';
import { getAllCategoriesQuery } from '../../../../graphql/category.query';
import Table from '../../table/table.component';



const CategoryTable = () => {

    const history = useHistory();

    const {loading, error, data, refetch} = useQuery(findCategoriesQuery, {
        variables: {input: {}}
    });

    const addCategoryClick = () => {
        history.push('/admin/category/create');
    }

    const updateCategoryClick = (id: number) => {
        history.push('/admin/category/edit/'+id);
    }

    const tableSettings: {columns: any} = {
        columns: {
            id: {
                title: 'Id'
            },
            title: {
                title: 'Title'
            },
            slug: {
                title: 'Slug'
            },
            category: {
                title: 'Category'
            }
        }
    }

    useEffect(() => {
		refetch();
	}, []);

    if(data) {
        console.log(data, 'data categories')
    }

    const getMappedData = () => {
        if(!data) return [];

        return data.findCategories.map((cat: any) => {
            return {
                ...cat,
                category: cat.category ? cat.category.title : '-'
            }
        });
    };

    if(loading) {
        return <div>Loading items...</div>
    }


    return (<div>
        <div><button onClick={addCategoryClick}>Add Category</button></div>
        <Table data={getMappedData()} tableSettings={tableSettings} onEdit={updateCategoryClick}></Table>
    </div>);
};


export default CategoryTable;