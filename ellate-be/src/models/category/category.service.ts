import { Injectable } from '@nestjs/common';
import { Category } from './category.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { BaseService } from '../core/base.service';
@Injectable()
export class CategoryService extends BaseService<Category> {
	constructor(@InjectRepository(Category) protected categoryRepository: Repository<Category>) {
		super(categoryRepository);
	}

	async findCategories(options: FindManyOptions<Category> ) {		
		return  this.categoryRepository.find({...options, relations: ['categories', 'category']});
	}

	async findById(id: string | number, findOptions: FindOneOptions<Category>): Promise<Category> {
		return this.categoryRepository.findOne(id, findOptions);
	}
}
