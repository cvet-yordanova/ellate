import React from "react";


import "./card-row.styles.scss";
import Card from "../card/card.component";
import IProduct from "../../models/IProduct";


const CardRow = (props: {products?: IProduct[]}) => {

    return <div className="et0-pr-row-box">
        <div className="et0-pr-row-label">
              <div></div>  
              <div>
                  <h1>Топ продукти</h1>
              </div>
        </div>
        <div className="et0-pr-row">
            {props.products && props.products.length ? props.products.map(product => {
                return <Card productData={product}></Card>;
            }) : null}
            
            {/* <Card></Card>
            <Card></Card>
            <Card></Card> */}
        </div>
    </div>

}


const mapDispatchToProps = (dispatch: any) => ({
});

export default CardRow;