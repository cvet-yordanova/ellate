import { apolloClient } from '../';
import { createCategoryQuery,
	 findCategoriesQuery } from '../graphql/category.query';
import { updateCategoryMutation } from '../graphql/category.query';

//tstodo type
export const createCategory = (createCategoryInput: any) => {
	return apolloClient.mutate({
		mutation: createCategoryQuery,
		variables: {input: createCategoryInput},
		optimisticResponse: true
	});
};

export const updateCategory = (updateCategoryInput: any) => {
	return apolloClient.mutate({
		mutation: updateCategoryMutation,
		variables: {input: updateCategoryInput},
		optimisticResponse: true
	});
};


export const findCategories = (findCategoriesInput = null) => {
	return apolloClient.query({
		query: findCategoriesQuery,
		variables: {input: findCategoriesInput}
	});
}
