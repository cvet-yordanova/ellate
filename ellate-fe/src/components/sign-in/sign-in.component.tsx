import React from "react";
import "./sign-in.styles.scss";
import FormInput from "../../components/form-input/form-input.component";
import CustomButton from "../../components/custom-button/custom-button.component";
import {signInWithGoogle} from "../.././firebase/firebase.utils";

class SignIn extends React.Component<any, any> {

    constructor(props: any) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }
    }

    handleSubmit = (event: any) => {
        event.preventDefault();

        this.setState({ email: '', password: '' });
    }

    handleChange = (event: any) => {
        const { value, name } = event.target;

        this.setState({ [name]: value });
    }

    render() {
        return (
            <div className="sign-in">
                <h2>I already have an account</h2>
                <span>Sign in with yout email and password</span>

                <form onSubmit={this.handleSubmit}>
                    <FormInput type="email"
                        name="email"
                        value={this.state.email}
                        required
                        handleChange={this.handleChange}
                        label="email" />
                    <FormInput
                        name="password"
                        type="password"
                        value={this.state.password}
                        required
                        handleChange={this.handleChange} 
                        label="password"/>
                    <FormInput
                        type="submit"
                        value="Submit form" 
                        label="submit"/>
                    <CustomButton type="submit">Sign In</CustomButton>    
                    <CustomButton onClick={signInWithGoogle} isGoogleSignIn>Sign In With Google</CustomButton>    
                </form>
            </div>
        );
    }
}

export default SignIn;