import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { IPagination } from "src/interfaces/IPagination";
import { UpdateResult } from "typeorm";
import { Product } from "./product.entity";
import { ProductService } from "./product.service";
@Resolver('Product')
export class ProductResolver {
  constructor(private productService: ProductService) {}

  @Query('allProducts')
  async allProducts(@Args() args: {input: any}): Promise<IPagination<Product>> {
    const {productOptions = null, searchOptions = null} = args.input;
    return this.productService.findProducts(productOptions, searchOptions);
  }

  @Query('getProductById')
  async getProductById(@Args() args: {input: string | number}): Promise<Product> {
    return this.productService.findProductById(args.input, {relations: ['category']});
  }

  @Query('getProductsByCategoryId')
  async getProductsByCategoryId(@Args() args: {input: string | number}): Promise<Product[]> {
    return this.productService.findProductsByCategoryId(+args.input);
  }

  //tstodo save image
  @Mutation('createProduct')
  async createProduct(@Args() args: any): Promise<any> {
    return await this.productService.create(args.input);
  }

  @Mutation('updateProduct')
  async updateProduct(@Args() args: any): Promise<UpdateResult> {
    const {input} = args;
    return await this.productService.update(input.id, input);
  }

}