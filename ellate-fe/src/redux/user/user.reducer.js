import { REGISTER_SUCCESS } from "../../actions/action-types";
import userActionTypes from "./user.types";


const INITIAL_STATE = {
    currentUser: null,
    token: null,
    favourites: []
}

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SET_CURRENT_USER':
            return {
                ...state,
                currentUser: action.payload
            }
        case REGISTER_SUCCESS: return {
            ...state,
            currentUser: action.payload
        } 
        case userActionTypes.LOGIN_USER_SUCCESS:
                 
        return {
            ...state,
            currentUser: action.payload,
            token: action.payload? '' : action.payload.token
        }

        case userActionTypes.ADD_PRODUCT_TO_FAVOURITES_SUCCESS:
                 
        return {
            ...state,
            favourites: [...state.favourites, action.payload]
        } 
    
        default:
            return state
    }
}

export default userReducer;