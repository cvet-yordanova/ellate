import IProduct from "./IProduct";

export default interface ICategory {
    id?: number;
    title: String;
    slug: String;
    image: String;
    category?: ICategory;
    categories?: ICategory[],
    products?: IProduct[];
}