import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseService } from '../core/base.service';
import { CreateUserInput, LoginUserInput } from "graphql.schema";
import { verifyToken } from "../../helpers/user.helper";
import { ProductService } from "./../product/product.service";
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { Basket } from '../basket/basket.entity';



@Injectable()
export class UserService extends BaseService<User> {
  constructor(
     @InjectRepository(User) protected userRepository: Repository<User>,
     @InjectRepository(Basket) protected basketRepository: Repository<Basket>,
     private productService: ProductService
  ) {
     super(userRepository);
  }

  async getUserById(id: string | number): Promise<any> {
    return this.repository.findOne(id, { relations: ["favouriteProducts"] });
  }

  async createUser(data: CreateUserInput): Promise<User> {

    const existingUser = await this.userRepository.findOne({email: data.email});

    if (existingUser) {
      throw new HttpException("user.exists", HttpStatus.BAD_REQUEST);
    }

    let password = await this.getPasswordHash(data.password);
    const basket = await this.basketRepository.save(new Basket());
    const user = Object.assign(new User(), {...data, password: password, basket});
    return await this.userRepository.save(user);
  }

  async login(data: LoginUserInput): Promise<any> {

    const { email, password } = data;

    if (!(email && password)) {
      throw new HttpException("input.required", HttpStatus.BAD_REQUEST);
    };

    try {
      const user = await this.userRepository.findOne({email: email},{relations: ["basket"]});

      if (user && (await bcrypt.compare(password, user.password as string))) {
        const token = jwt.sign(
          { user_id: user.id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "4h",
          }
        );

        return Promise.resolve({...user, token: token});
      }
    } catch(err) {
      console.log(err, 'err')
      throw new HttpException("unknown.error", HttpStatus.BAD_REQUEST);
    }

  }

  async addProductToFavourites(productId, context) {

    const decoded = verifyToken(context);
    const user = await this.getUserById(decoded.user_id);

    if(!user) {
      throw new HttpException("user.not.found", HttpStatus.BAD_REQUEST);
    }

    const product = await this.productService.findProductById(productId, {});

    if(!product) {
      throw new HttpException("product.not.found", HttpStatus.BAD_REQUEST);
    }

    user.favouriteProducts.push(product);

    try {
     const res = await this.userRepository.save(user);

     return {   code: HttpStatus.OK,
                successMessage: "product.added.to.favourites"};

    } catch(err) {
      throw new HttpException("unknown.error", HttpStatus.BAD_REQUEST);
    }

  }

  public async getPasswordHash(password: string): Promise<string> {
		return bcrypt.hash(password, +process.env.HASH_SECRET);
	}
  
}