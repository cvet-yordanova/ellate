import { gql } from '@apollo/client';
import { apolloClient } from '..';
import { loginUserMutation, registerUserMutation } from '../graphql/user.query';
import { ILogin } from '../redux/user/user.actions';

// const client = ...

export const registerNewUser = (userData: any) => {
      return apolloClient.mutate({
        mutation: registerUserMutation,
        variables: {input: userData},
        optimisticResponse: true
      })
}

export const loginUser = (user: ILogin) => {
  return apolloClient.mutate({
    mutation: loginUserMutation,
    variables: {input: user}
  });
}

export const testQuery = gql`
   query Test {
    test
  }
`;



export const testFunction = () => {
    apolloClient.query({
                query: gql`
                    query Test{                        
                          allUsers{
                            firstName
                          }                      
                      } 
                `
            })
        .then(result => console.log(result));
}

