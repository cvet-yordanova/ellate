import React, { useEffect, useState } from 'react';
import CardRow from '../../components/card-row';
import Footer from '../../components/footer';
import LoginModal from '../../components/modals/login-modal';
import PageContainer from '../../components/page-container';
import TopMenu from '../../components/top-menu';
import Header from '../../components/header';

import './category-page.styles.scss';
import IBaseProps from '../../models/IBaseProps';
import { useDispatch, useSelector } from 'react-redux';
import { loadCategoryPageProducts } from '../../redux/pages/category/category-page.actions';


const CategoryPage = (props: IBaseProps) => {

	const selectedData = useSelector((state: any) => state.shopping);
    const categoryPage = useSelector((state: any) => state.categoryPage.categories[props.match.params.categoryId]);
    const [productsOnPage, setProducts] = useState([]);
    const dispatch = useDispatch();


    useEffect(() => {
        if(categoryPage && categoryPage.productIds ) {
            setProducts(categoryPage.productIds.map((id: any) => selectedData.products[id]));
        }

    }, [selectedData, categoryPage]);

    useEffect(() => {
        if(!categoryPage){
            loadCategoryPageProducts(props.match.params.categoryId, categoryPage, dispatch);
        }

    }, [props.match.params.categoryId]);



    return(
        <div>
        <div>
            <LoginModal />
        </div>
        <Header />
        <TopMenu />
        <PageContainer>
            <CardRow products={productsOnPage} />

        </PageContainer>
        <Footer />
    </div>
    )
}

export default CategoryPage;