import React from 'react';
import './carousel-slides.styles.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import CarouselSlide from '../carousel-slide/carousel-slide.component';

import slideImage1 from './../../assets/images/banner/pic1.jpg';
import slideImage2 from './../../assets/images/banner/pic2.jpg';
import slideImage3 from './../../assets/images/banner/pic3.jpg';
import slideImage4 from './../../assets/images/banner/pic4.jpg';

const CarouselSlidesComponent = () => {
	return (
		<Swiper
			spaceBetween={0}
			slidesPerView={1}
			onSlideChange={() => console.log('slide change')}
			onSwiper={(swiper) => console.log(swiper)}
		>
			<SwiperSlide><CarouselSlide imageUrl={slideImage1}/></SwiperSlide>
			<SwiperSlide><CarouselSlide imageUrl={slideImage2}/></SwiperSlide>
			<SwiperSlide><CarouselSlide imageUrl={slideImage3}/></SwiperSlide>
			<SwiperSlide><CarouselSlide imageUrl={slideImage4}/></SwiperSlide>
			...
		</Swiper>
	);
};

export default CarouselSlidesComponent;
