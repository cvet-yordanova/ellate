import BasketActionTypes from "./basket.types";


export interface IBasketState {
    items: IBasketItem[],
    total: number,
}

export interface IBasketItem {
    productId: string | number,
    quantity: number,
    total: number
}


const INITIAL_STATE: IBasketState = {
    items: [],
    total: 0
};


const basketReducer = (state = INITIAL_STATE, action: {type: string, payload: any}) => {

    switch(action.type) {

        case BasketActionTypes.UPDATE_BASKET_SUCCESS:

        const basket = action.payload.basket;

        return {
            items: basket.lineItems,
            total: action.payload.total
        }

                   
        default: return state;
    }
}

export default basketReducer;