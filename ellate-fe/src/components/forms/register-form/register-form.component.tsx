import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

import './register-form.styles.scss';
import { registerUser } from '../../../redux/user/user.actions';
import { useDispatch } from 'react-redux';

const RegisterForm = () => {

  const disptach = useDispatch();

  return <div className="et0-register-form">
    <h1 className="et0-form-label">Регистрация</h1>
    <Formik
      initialValues={{ firstName: '', lastName: '', email: '', phone: '',  password: '', cofirmPassword: '' }}
      validate={values => {
        const errors: any = {};
        if (!values.email) {
          errors.email = 'Required';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Invalid email address';
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          setSubmitting(false);
        }, 400);

        const data = {
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          phone: values.phone,
          password: values.password};

          registerUser(data, disptach);

      }}
    >
      {({ isSubmitting }) => (
        <Form > 
          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Име</label>
            <Field className="et0-input-field" type="text" name="firstName" />
            <ErrorMessage name="email" component="div" />
          </div>

          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Фамилия</label>
            <Field className="et0-input-field" type="text" name="lastName" />
            <ErrorMessage name="email" component="div" />
          </div>

          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Имейл</label>
            <Field className="et0-input-field" type="email" name="email" />
            <ErrorMessage name="email" component="div" />
          </div>

          
          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Телефон</label>
            <Field className="et0-input-field" type="phone" name="phone" />
            <ErrorMessage name="email" component="div" />
          </div>


          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Парола</label>
            <Field className="et0-input-field" type="password" name="password" />
            <ErrorMessage name="password" component="div" />
          </div>

          <div className="et0-form-field">
            <label className="et0-field-label" htmlFor="">Повтори парола</label>
            <Field className="et0-input-field" type="password" name="repeatPassword" />
            <ErrorMessage name="password" component="div" />
          </div>

          <button type="submit" disabled={isSubmitting}>
            Submit
           </button>
        </Form>
      )}
    </Formik>
  </div>
};

export default RegisterForm;