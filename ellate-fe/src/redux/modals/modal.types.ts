const modalActionTypes = {
    SHOW_LOGIN_MODAL: 'SHOW_LOGIN_MODAL',
    HIDE_LOGIN_MODAL: 'HIDE_LOGIN_MODAL',
    HIDE_ALL_MODALS: 'HIDE_ALL_MODALS',
    CLOSE_MODAL: 'CLOSE_MODAL'
}

export default modalActionTypes;