import React from 'react';
import "./registration-page.styles.scss";
import PageContainer from '../../components/page-container';
import RegisterForm from '../../components/forms/register-form';
import Footer from '../../components/footer';



const RegistrationPage = () => {
    return <div className="et0-registration-page">
        <PageContainer className="et0-registration-page">
            <div className="et0-registration-form-box">
                <RegisterForm></RegisterForm>
            </div>
        </PageContainer>

        <Footer></Footer>
    </div>



}


export default RegistrationPage;