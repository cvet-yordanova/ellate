import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseService } from '../core/base.service';
import { Product } from './product.entity';
import { IPagination } from 'src/interfaces/IPagination';

@Injectable()
export class ProductService extends BaseService<Product> {
	constructor(@InjectRepository(Product) protected productRepository: Repository<Product>) {
		super(productRepository);
	}

	//tstodo
	async findProducts(productOptions: any, searchOptions: any): Promise<IPagination<Product>> {
		const count = await this.productRepository.count({ where: productOptions });

		const products = await this.productRepository.find({
			where: productOptions,
			relations: searchOptions.relations,
			skip: (searchOptions.page - 1) * searchOptions.perPage,
			take: searchOptions.perPage
		});

		return { total: count, items: products };
	}

	async findProductById(id: string | number, searchOptions: any) {
		return this.productRepository.findOne(id, { relations: searchOptions.relations });
	}
             
	//tstodo add relations in options obj
	async findProductsByCategoryId(categoryId: string | number) {
		return this.productRepository.find({relations: ['category'], where: {deleted: false, category: { id: categoryId } } }); 
	}
}
