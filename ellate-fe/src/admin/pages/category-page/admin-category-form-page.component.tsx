import React from 'react';
import PageContainer from '../../../components/page-container';
import AdminHeaderComponent from '../../components/admin-header';
import CategoryForm from '../../components/forms/category-form';
import './admin-category-form-page.styles.scss';

const AdminCategoryFormPage = () => {
	return (
		<div>
			<AdminHeaderComponent/>
			<PageContainer>
				<CategoryForm />
			</PageContainer>
		</div>
	);
};

export default AdminCategoryFormPage;
