import React from 'react';
import PageContainer from '../../../../components/page-container';
import AdminHeaderComponent from '../../../components/admin-header';
import CategoryTable from '../../../components/tables/category-table';


const AdminCategoriesTablePage = () => {

    return (
        <div>
            <AdminHeaderComponent/>
            <PageContainer>
                <CategoryTable/>
            </PageContainer>
        </div>
    )
};

export default AdminCategoriesTablePage;