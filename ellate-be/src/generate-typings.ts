import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['./src/models/**/*.graphql'],
  path: join(process.cwd(), './graphql.schema.ts'),
  outputAs: 'class',
  watch: true
});