
import { registerNewUser, loginUser  } from "../../services/auth.service";
import { addProductToFavourites } from "../../services/user.service";
import userActionTypes from "./user.types";
export interface ILogin {
    email: string,
    password: string
}

export const login = (user: ILogin, dispatchFunction: Function): Promise<any> => {
	dispatchFunction({type: userActionTypes.LOGIN_USER});

   return loginUser(user).then((res) => {
       const {data} = res;

        if (res && res.data && res.data.login) {
            localStorage.setItem('token', data.login.token);
            dispatchFunction({type: userActionTypes.LOGIN_USER_SUCCESS, payload: data.login});
            return Promise.resolve(true);
        } else {
            Promise.reject(false);
        };

    });

}

export const addToFavourites = (productId: string, dispatchFunction: Function): Promise<any> => {
	dispatchFunction({type: userActionTypes.ADD_PRODUCT_TO_FAVOURITES});

   return addProductToFavourites(productId).then((res: any) => {
       const {data} = res;

       //tstodo
       console.log(data.addProductToFavourites as any, 'res');

        if (data && data.addProductToFavourites && data.addProductToFavourites.code == "200") {
            dispatchFunction({type: userActionTypes.ADD_PRODUCT_TO_FAVOURITES_SUCCESS, payload: productId});
            return Promise.resolve(true);
        } else {
            Promise.reject(false);
        };

    });

}


export const setCurrentUser = (user: any) => ({
    type: "SET_CURRENT_USER",
    payload: user
});

export const register = (user: any) => ({
    type: "REGISTER USER",
    payload: user
});

export const registerUserAction = () => ({
    type: userActionTypes.REGISTER_USER
});


export const registerUserSuccess = (user: any) => ({
    type: userActionTypes.REGISTER_USER_SUCCESS,
    payload: user
});

export const registerUserFail = (error: any) => ({
    type: userActionTypes.REGISTER_USER_FAIL,
    payload: error
});

export const loginUserAction = () => ({
    type: userActionTypes.LOGIN_USER
});

export const loginUserSuccess = (user: any) => ({
    type: userActionTypes.LOGIN_USER_SUCCESS,
    payload: user
});

export const loginUserFail = (error: any) => ({
    type: userActionTypes.REGISTER_USER_FAIL,
    payload: error
});

export interface IRegisterUser {
        firstName: String,
        lastName: String,
        phone: String,
        email: String,
        password: String
}


export const registerUser = (user: IRegisterUser, dispatchFunction: any) => {
    dispatchFunction({type: userActionTypes.REGISTER_USER});
    
    registerNewUser(user).then(res => {
        console.log(res, 'res from registering user');
    });

         
}