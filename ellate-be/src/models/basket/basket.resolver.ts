import { Args, Context, Mutation, Resolver } from "@nestjs/graphql";
import { BasketService } from "./basket.service";

@Resolver('Basket')
export class BasketResolver {


    constructor(private basketService: BasketService) { }

    @Mutation('addProductToBasket')
    async addProductToBasket(@Args() args: any, @Context() context: any) {
      const {input} = args;
      //tstodo
      console.log(input, 'input')
      return await this.basketService.addProductToBasket(input.productId, context);
    }


}