import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Product } from "../product/product.entity";
import { Category } from "./category.entity";
import { CategoryResolver } from "./category.resolver";
import { CategoryService } from "./category.service";


@Module({
    imports: [TypeOrmModule.forFeature([Category, Product])],
    providers: [CategoryResolver, CategoryService],
    exports: [CategoryService]
  })
  export class CategoryModule {}