import { apolloClient } from "..";
import { addProductToBasketMutation } from "../graphql/basket.query";


export const addProductToBasket = (productInput: any) => {
    return apolloClient.mutate({
        mutation: addProductToBasketMutation,
        variables: {input: {productId: productInput}},
        optimisticResponse: true
    });
};