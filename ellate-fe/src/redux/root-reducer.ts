import { combineReducers } from 'redux';

import userReducer from './user/user.reducer';
import modalReducer from './modals/modal.reducer';
import notificationReducer from './notification/notification.reducer';
import productReducer from './products/product.reducer';
import categoryPageReducer from './pages/category/category-page.reducer';
import basketReducer from './basket/basket.reducer';

export default combineReducers({
  user: userReducer,
  shopping: productReducer,
  basket: basketReducer,
  modal: modalReducer,
  notification: notificationReducer,
  categoryPage: categoryPageReducer
});