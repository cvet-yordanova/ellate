import { apolloClient } from ".."
import { getProductByIdQuery, getProductsByCategoryIdQuery, updateProductMutation } from "../graphql/product.query";
import { createProductMutation } from "../graphql/product.query";

export const getProductById = (productId: string) => {
    return apolloClient.query({
        query: getProductByIdQuery,
        variables: {input: productId}
    });
}

export const createProduct = (createProductInput: any) => {
    return apolloClient.mutate({
        mutation: createProductMutation,
        variables: {input: createProductInput},
        optimisticResponse: true
    });
};

export const updateProduct = (updateProductInput: any) => {
    return apolloClient.mutate({
        mutation: updateProductMutation,
        variables: {input: updateProductInput},
        optimisticResponse: true
    });
};


export const getProductsByCategoryId = (categoryId: string) => {
    return apolloClient.query({
        query: getProductsByCategoryIdQuery,
        variables: {input: categoryId}
    })
}